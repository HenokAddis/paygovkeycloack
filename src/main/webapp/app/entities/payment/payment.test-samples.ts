import { IPayment, Payment } from './payment.model';

export const sampleWithRequiredData: IPayment = {
  id: 47537,
  cik: 'connect RSS eco-centric',
  ccc: 'functionalities Account',
  paymentAmount: 'Cotton',
  name: 'microchip strategic methodical',
  email: 'Pg0L',
  phone: '1-667-922-4896 x46084',
  approvalStatus: 'Account',
};

export const sampleWithPartialData: IPayment = {
  id: 17394,
  cik: 'connect reintermediate green',
  ccc: 'encryption hack',
  paymentAmount: 'Sleek',
  name: 'Books',
  email: 'sYnC',
  phone: '1-676-387-6610 x93066',
  approvalStatus: 'Borders Fundamental',
};

export const sampleWithFullData: IPayment = {
  id: 42002,
  cik: 'Computers Cotton Developer',
  ccc: 'portal Group FTP',
  paymentAmount: 'distributed aggregate',
  name: 'Shoes loyalty',
  email: 'c',
  phone: '507.736.4971',
  approvalStatus: 'Cameroon RSS',
};

export const sampleWithNewData: Payment = {
  cik: 'Unbranded',
  ccc: 'Bedfordshire deposit engineer',
  paymentAmount: 'Salad',
  name: 'innovate array Plaza',
  email: '0ZBbc',
  phone: '(625) 794-5202 x815',
  approvalStatus: 'Swaziland deposit transmitter',
  id: 0,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
