import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PaymentComponent } from '../list/payment.component';
import { PaymentDetailComponent } from '../detail/payment-detail.component';
import { PaymentUpdateComponent } from '../update/payment-update.component';
import { PaymentRoutingResolveService } from './payment-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';
import { KafkadminComponent } from '../kafkadmin/kafkadmin.component';
import { PaymentCreateDialogComponent } from '../create/payment-create-dialog.component';

const paymentRoute: Routes = [
  // {
  //   path: '',
  //   component: PaymentComponent,
  //   data: {
  //     defaultSort: 'id,' + ASC,
  //   },
  //   canActivate: [UserRouteAccessService],
  // },
  // {
  //   path: ':id/view',
  //   component: PaymentDetailComponent,
  //   resolve: {
  //     payment: PaymentRoutingResolveService,
  //   },
  //   canActivate: [UserRouteAccessService],
  // },
  {
    path: 'list',
    component: PaymentComponent,
    // canActivate: [UserRouteAccessService],
  },

  {
    path: 'kafka',
    component: KafkadminComponent,
    // canActivate: [UserRouteAccessService],
  },

  {
    path: 'view',
    component: PaymentDetailComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
    // canActivate: [UserRouteAccessService],
  },
  {
    path: 'delete',
    component: PaymentCreateDialogComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
    // canActivate: [UserRouteAccessService],
  },

  {
    path: 'new',
    component: PaymentUpdateComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
    // canActivate: [UserRouteAccessService],
  },
  {
    path: '',
    component: PaymentUpdateComponent,
    resolve: {
      payment: PaymentRoutingResolveService,
    },
    // canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(paymentRoute)],
  exports: [RouterModule],
})
export class PaymentRoutingModule {}
