export interface IPayment {
  id?: number;
  cik?: string | null;
  ccc?: string | null;
  paymentAmount?: string | null;
  name?: string | null;
  email?: string | null;
  phone?: string | null;
  approvalStatus?: string | null;
}

export class Payment implements IPayment {
  constructor(
    public id?: number,
    public cik?: string | null,
    public ccc?: string | null,
    public paymentAmount?: string | null,
    public name?: string | null,
    public email?: string | null,
    public phone?: string | null,
    public approvalStatus?: string | null
  ) {}
}

export function getPaymentIdentifier(payment: IPayment): number | undefined {
  return payment.id;
}

// export interface IPayment {
//   id: number;
//   cik?: string | null;
//   ccc?: string | null;
//   paymentAmount?: string | null;
//   name?: string | null;
//   email?: string | null;
//   phone?: string | null;
//   approvalStatus?: string | null;
// }

// export type NewPayment = Omit<IPayment, 'id'> & { id: null };
